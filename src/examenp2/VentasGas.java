/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenp2;

/**
 *
 * codigo   cantidad    tipo premium o regular
 * pLitro
 * totalP
 * @author Javier Duran
 */
public class VentasGas {
    
    private int codigo;
    private float cantidad;
    private int tipo;
    private float pLitro;
    
    
    public VentasGas() {
        this.codigo = 0;
        this.cantidad = 0.0f;
        this.tipo = 0;
        this.pLitro = 0.0f;
    }
    
    public VentasGas(int codigo, float cantidad, int tipo, float pLitro) {
        this.codigo = codigo;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.pLitro = pLitro;
    }
    
    public VentasGas(VentasGas otro) {
        this.codigo = otro.codigo;
        this.cantidad = otro.cantidad;
        this.tipo = otro.tipo;
        this.pLitro = otro.pLitro;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getpLitro() {
        return pLitro;
    }

    public void setpLitro(float pLitro) {
        this.pLitro = pLitro;
    }
  /*
    public float calcularPrecio() {
        float precio = 0.0f;
        if(tipo==1){
            precio = 20.50f;
        }
        if(tipo==2){
            precio = 24.50f;
        }
        return precio;
    }
  */
    
    public float calcularTotal() {
        float total = 0.0f;
        total = pLitro * cantidad;
        return total;
    
    }
}
    
    
    
    
